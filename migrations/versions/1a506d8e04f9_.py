"""

Revision ID: 1a506d8e04f9
Revises: 81ec8e2dfa43
Create Date: 2022-02-20 16:52:47.194447

"""

# revision identifiers, used by Alembic.
revision = "1a506d8e04f9"
down_revision = "81ec8e2dfa43"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("components", sa.Column("device_integrity", sa.Text(), nullable=True))
    op.add_column("protocol", sa.Column("is_transport", sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column("protocol", "is_transport")
    op.drop_column("components", "device_integrity")
