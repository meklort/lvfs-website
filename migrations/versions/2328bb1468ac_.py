"""

Revision ID: 2328bb1468ac
Revises: 94298233f61b
Create Date: 2021-02-25 16:30:03.125813

"""

# revision identifiers, used by Alembic.
revision = '2328bb1468ac'
down_revision = '94298233f61b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('firmware', sa.Column('restricted_ts', sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column('firmware', 'restricted_ts')
