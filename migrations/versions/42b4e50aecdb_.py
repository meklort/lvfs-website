"""

Revision ID: 42b4e50aecdb
Revises: 962f3295b5ea
Create Date: 2022-04-21 13:37:07.982329

"""

# revision identifiers, used by Alembic.
revision = "42b4e50aecdb"
down_revision = "962f3295b5ea"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index(op.f("ix_remotes_name"), "remotes", ["name"], unique=False)


def downgrade():
    op.drop_index(op.f("ix_remotes_name"), table_name="remotes")
