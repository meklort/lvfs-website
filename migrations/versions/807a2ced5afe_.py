"""

Revision ID: 807a2ced5afe
Revises: 7d2c4321b2ab
Create Date: 2020-11-27 10:15:20.387164

"""

# revision identifiers, used by Alembic.
revision = '807a2ced5afe'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('vendors', sa.Column('fws_stable', sa.Integer(), nullable=True))
    op.add_column('vendors', sa.Column('fws_stable_recent', sa.Integer(), nullable=True))
    op.add_column('vendors', sa.Column('is_odm', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('vendors', 'is_odm')
    op.drop_column('vendors', 'fws_stable_recent')
    op.drop_column('vendors', 'fws_stable')
