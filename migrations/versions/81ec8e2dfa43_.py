"""

Revision ID: 81ec8e2dfa43
Revises: e8e15b84a9aa
Create Date: 2022-02-04 14:08:35.401686

"""

# revision identifiers, used by Alembic.
revision = "81ec8e2dfa43"
down_revision = "e8e15b84a9aa"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "products",
        sa.Column("product_id", sa.Integer(), nullable=False),
        sa.Column("appstream_id", sa.Text(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("mtime", sa.DateTime(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("state", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("product_id"),
    )
    op.create_index(
        op.f("ix_products_appstream_id"), "products", ["appstream_id"], unique=False
    )
    op.create_index(op.f("ix_products_state"), "products", ["state"], unique=False)


def downgrade():
    op.drop_index(op.f("ix_products_state"), table_name="products")
    op.drop_index(op.f("ix_products_appstream_id"), table_name="products")
    op.drop_table("products")
