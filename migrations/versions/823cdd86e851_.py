"""

Revision ID: 823cdd86e851
Revises: dfa8440c4f38
Create Date: 2021-06-10 12:14:02.102536

"""

# revision identifiers, used by Alembic.
revision = "823cdd86e851"
down_revision = "dfa8440c4f38"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("fscks", sa.Column("extended", sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column("fscks", "extended")
