"""

Revision ID: 8a8e71983bb3
Revises: e7acaaafdf13
Create Date: 2020-12-16 15:11:12.835101

"""

# revision identifiers, used by Alembic.
revision = '8a8e71983bb3'
down_revision = 'e7acaaafdf13'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('clients', sa.Column('country_code', sa.Text(), nullable=True))


def downgrade():
    op.drop_column('clients', 'country_code')
