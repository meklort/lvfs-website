"""

Revision ID: 950d171bc774
Revises: 73fbdf11b2fc
Create Date: 2021-03-31 09:53:51.770835

"""

# revision identifiers, used by Alembic.
revision = '950d171bc774'
down_revision = '73fbdf11b2fc'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('vendors', sa.Column('default_inf_parsing', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('vendors', 'default_inf_parsing')
