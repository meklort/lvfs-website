"""

Revision ID: f80b9c7b7267
Revises: ec2fe30db198
Create Date: 2021-11-24 07:23:45.615896

"""

# revision identifiers, used by Alembic.
revision = "f80b9c7b7267"
down_revision = "ec2fe30db198"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("licenses", sa.Column("url", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("licenses", "url")
