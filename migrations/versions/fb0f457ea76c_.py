"""

Revision ID: fb0f457ea76c
Revises: 4cab6062645f
Create Date: 2021-05-05 10:26:53.818024

"""

# revision identifiers, used by Alembic.
revision = 'fb0f457ea76c'
down_revision = '4cab6062645f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('fscks',
    sa.Column('fsck_id', sa.Integer(), nullable=False),
    sa.Column('container_id', sa.Text(), nullable=True),
    sa.Column('started_ts', sa.DateTime(), nullable=False),
    sa.Column('ended_ts', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('fsck_id')
    )
    op.create_table('fsck_attributes',
    sa.Column('fsck_attribute_id', sa.Integer(), nullable=False),
    sa.Column('fsck_id', sa.Integer(), nullable=False),
    sa.Column('title', sa.Text(), nullable=False),
    sa.Column('message', sa.Text(), nullable=True),
    sa.Column('success', sa.Boolean(), nullable=True),
    sa.ForeignKeyConstraint(['fsck_id'], ['fscks.fsck_id'], ),
    sa.PrimaryKeyConstraint('fsck_attribute_id')
    )
    op.create_index(op.f('ix_fsck_attributes_fsck_id'), 'fsck_attributes', ['fsck_id'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_fsck_attributes_fsck_id'), table_name='fsck_attributes')
    op.drop_table('fsck_attributes')
    op.drop_table('fscks')
